// Soal 1
console.log('SOAL 1 -----------');
console.log('LOOPING PERTAMA');
var deret = 0;
while (deret < 20) {
  deret += 2;
  console.log(deret,"-","i love coding");
}
console.log('LOOPING KEDUA');
var deret2 = 20;
while (deret >= 2) {
  console.log(deret,"-","i love coding");
  deret -= 2;
}
console.log(' ');
// Soal 2
console.log('SOAL 2 -----------');
for (var angka = 1; angka <= 20; angka++) {
  if (angka % 3 == 0 && angka % 2 !== 0) {
    console.log(angka, "-", "I Love Coding");
  } else if (angka % 2 == 0){
    console.log(angka, "-", "Berkualitas");
  } else {
    console.log(angka, "-", "Santai");
  }
}
console.log(' ');
// Soal 3
console.log('SOAL 3 -----------');
for (var i2 = 0; i2 < 4; i2++) {
	var tmp = '';
	for (var j2 = 0; j2 < 8; j2++) {
    tmp = tmp+'#';
	}
	console.log(tmp);
}
console.log(' ');
// Soal 4
console.log('SOAL 4 -----------');
for (var ii = 1; ii <= 7; ii++) {
	var num = '';
	for (var jj = 1; jj <= ii; jj++) {
		num += '#';
	}
	console.log(num);
}
console.log(' ');
// Soal 5
console.log('SOAL 5 -----------');
var hasil='';
for(var i8=0;i8<8;i8++){
  for(var j8=0;j8<8;j8++){
    if(i8%2==1){
      if(j8%2==0){
        hasil=hasil+'#';
      } else {
        hasil=hasil+' ';
      }
    } else {
      if(j8%2==0){
        hasil=hasil+' ';
      } else {
        hasil=hasil+'#';
      }
    }
  }
  hasil=hasil;
  console.log(hasil);
  hasil='';
}
