var nama = 'Jenita'
var peran = 'Werewolf'

if (nama == '') {
    console.log('Nama harus diisi!')
} else if (peran == '') {
    console.log('Halo John, Pilih peranmu untuk memulai game!')
} else if (peran == 'Penyihir') {
    console.log('Selamat datang di Dunia Werewolf, Jane')
    console.log('Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!')
} else if (peran == 'Guard') {
    console.log('Selamat datang di Dunia Werewolf, Jenita')
    console.log('Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.')
} else if (peran == 'Werewolf') {
    console.log('Selamat datang di Dunia Werewolf, Junaedi')
    console.log('Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!')
}

var tanggal = 7; 
var bulan = 3; 
var tahun = 1918;
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945';
switch(bulan) {
    case 1: { var bulan = 'Januari'; break; }
    case 2: { var bulan = 'Februari'; break; }
    case 3: { var bulan = 'Maret'; break; }
    case 4: { var bulan = 'April'; break; }
    case 5: { var bulan = 'Mei'; break; }
    case 6: { var bulan = 'Juni'; break; }
    case 7: { var bulan = 'Juli'; break; }
    case 8: { var bulan = 'Agustus'; break; }
    case 9: { var bulan = 'September'; break; }
    case 10: { var bulan = 'Oktober'; break; }
    case 11: { var bulan = 'November'; break; }
    case 12: { var bulan = 'Desember'; break; }
}
if (tanggal<1||tanggal>31) {
    console.log('tanggal harus dengan angka antara 1 - 31'); 
} else if (bulan<1||bulan>12) {
    console.log('bulan harus dengan angka antara 1 - 12')
} else if (tahun<1900||tahun>2200) {
    console.log('tahun harus dengan angka antara 1900 - 2200')
} else{
    console.log(tanggal, bulan, tahun)
}