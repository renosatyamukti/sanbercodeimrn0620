console.log('SOAL 1 -----------');
// Soal 1
function range(startNum, finishNum) {
  var numbers=[]
  var op = "-1";
  if (startNum == null || finishNum == null) {
    return op;
  } else {
    if (startNum < finishNum) {
      while (startNum <= finishNum) {
        numbers.push(startNum);
        startNum = startNum + 1;
      }
      return numbers;
    } else {
      while (startNum >= finishNum) {
        numbers.push(startNum);
        startNum = startNum - 1;
      }
      return numbers;
    }
  }
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log(' ');
console.log('SOAL 2 -----------');
// Soal 2
function rangeWithStep(startNum, finishNum, step) {
  var numbers = [];
  var op = "-1";
  if (startNum == null || finishNum == null) {
    return op;
  } else {
    if (startNum < finishNum) {
      while (startNum <= finishNum) {
        numbers.push(startNum);
        startNum = startNum + step;
      }
      return numbers;
    } else {
      while (startNum >= finishNum) {
        numbers.push(startNum);
        startNum = startNum - step;
      }
      return numbers;
    }
  }
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log(' ');
console.log('SOAL 3 -----------');
// Soal 3
function sum(startNum, finishNum, step) {
  var numbers = [];
  if (startNum == null && finishNum == null && step == null) {
    return 0;
  } else if (finishNum == null && step == null) {
    return startNum;
  } else {
    if (step == null) {
      var step = 1;
      if (startNum < finishNum) {
        while (startNum <= finishNum) {
          numbers.push(startNum);
          // var hasil = startNum + startNum;
          startNum = startNum + step;
        }
        var ss = numbers.reduce(function (a, b) {
          return a + b;
        }, 0);
        return ss;
      } else {
        while (startNum >= finishNum) {
          numbers.push(startNum);
          startNum = startNum - step;
        }
        var ss = numbers.reduce(function (a, b) {
          return a + b;
        }, 0);
        return ss;
      }
    } else {
      if (startNum < finishNum) {
        while (startNum <= finishNum) {
          numbers.push(startNum);
          // var hasil = startNum + startNum;
          startNum = startNum + step;
        }
        var ss = numbers.reduce(function (a, b) {
          return a + b;
        }, 0);
        return ss;
      } else {
        while (startNum >= finishNum) {
          numbers.push(startNum);
          startNum = startNum - step;
        }
        var ss = numbers.reduce(function (a, b) {
          return a + b;
        }, 0);
        return ss;
      }
    }
  }
}
console.log(sum(1, 10, 1)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log(' ');
console.log('SOAL 4 -----------');
// Soal 4

// var noId=''
// var nama=''
// var ttl=''
// var hobi=''

function dataHandling(input2) {
  var jaa = 0;
  for (var aj = 0; aj <= input2.length; aj++) {
    console.log("Nomor ID:", input2[jaa][0]);
    console.log("Nama:", input2[jaa][1]);
    console.log("TTL:", input2[jaa][2], input2[jaa][3]);
    console.log("Hobi:", input2[jaa][4]);
    console.log(" ");
    jaa = jaa + 1;
  }
}
// function dataHandling(input2) {
//   var jaa = 0;
//   var nol = 0;
//   var satu = 1;
//   var dua = 2;
//   var tiga = 3;
//   var mpat = 4;
//   for (var aj = 0; aj <= input2.length; aj++) {
//     var noId = input2[jaa][nol];
//     var nama = input2[jaa][satu];
//     var ttl = input2[jaa][dua];
//     var ttl2 = input2[jaa][tiga];
//     var hobi = input2[jaa][mpat];
//     console.log("Nomor ID:", noId);
//     console.log("Nama:", nama);
//     console.log("TTL:", ttl, ttl2);
//     console.log("Hobi:", hobi);
//     console.log(" ");
//     jaa = jaa + 1;
//     var iuo = "aaa";
//   }
//   return 0;
// }

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
console.log(dataHandling(input))
console.log('aaaaaaaaa')